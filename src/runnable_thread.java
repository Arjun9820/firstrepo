public class runnable_thread {
    public static void main(String args[])
    {
        ThreadA a=new ThreadA("/initializing thread A");
        ThreadB b=new ThreadB("/Initializing thread B");
        a.start();
        b.start();
    }
}

class ThreadA extends Thread
{
    String name;
    ThreadA(String n)
    {
        name=n;
    }
    public void run()
    {
        System.out.println("Running"+name);
        try
        {
            for(int i=0;i<=5;i++)
            {
                System.out.println(i);
                Thread.sleep(1000);
            }
        }catch(Exception e)
        {
            System.out.println(e);
        }
    }
}

class ThreadB extends Thread
{
    String name;
    ThreadB(String n)
    {
        name=n;
    }
    public void run()
    {
        System.out.println("Running"+name);
        try
        {
            for(int i=0;i<=5;i++)
            {
                System.out.println(i);
                Thread.sleep(1000);
            }
        }catch(Exception e)
        {
            System.out.println(e);
        }
    }
}

